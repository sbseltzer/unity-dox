$ASSETS_PATH = (Get-ChildItem "../" -Recurse -filter "Assets" -Directory -Name | Select-Object -First 1)
$UNITY_PROJECT = (get-item "..\$ASSETS_PATH" ).parent.Name
$project_path = "..\$UNITY_PROJECT"
$ASSETS_PATH = "$project_path\Assets"
$env:UNITY_PROJECT = $UNITY_PROJECT
$env:ASSETS_PATH = $ASSETS_PATH
$env:SCRIPTS_PATH = "$ASSETS_PATH\Scripts"
doxygen Doxyfile
echo $env:UNITY_PROJECT
echo $env:ASSETS_PATH
echo $env:SCRIPTS_PATH
