#!/bin/bash
DOX_DIR="`echo ${PWD##*/}`"
cd ..
ASSETS_PATH=`find */Assets -print -quit`
export UNITY_PROJECT=`dirname $ASSETS_PATH`
cd "$DOX_DIR"
export ASSETS_PATH="../$UNITY_PROJECT/Assets"
export SCRIPTS_PATH="$ASSETS_PATH/Scripts"
doxygen Doxyfile
echo $UNITY_PROJECT
echo $ASSETS_PATH
echo $SCRIPTS_PATH
